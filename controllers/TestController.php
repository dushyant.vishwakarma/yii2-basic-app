<?php

namespace app\controllers;

use yii\web\Controller;

class TestController extends Controller{

    public function actionServer(){
        echo "<h1>Hello World</h1>";
    }

    public function actionTesting(){
        $name = "Marco";
        $number = 12345;
        $arr = ["BMW","Ferrari","Porche"];
        return $this->render('testing',['name' => $name,'number' => $number,'arr' => $arr]);
    }
    
}